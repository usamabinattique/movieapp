//
//  MovieCell.swift
//  MovieApp
//
//  Created by Usama bin Attique on 27/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var releaseYearLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        descriptionTextView.textContainerInset = UIEdgeInsets(top: 1, left: 0, bottom: 2, right: 5)
        descriptionTextView.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail


    }
    
    
    func configureCell(movieObject: Movie){
        
        titleLabel.text = movieObject.title
        
        let dateFromString = Date().getDateFromString(dateString: movieObject.releaseDate!)
        
        let releaseYear = Date().getYear(dateFromString!)
        
        releaseYearLabel.text = "(" + releaseYear + ")"
        
        descriptionTextView.text = movieObject.description
        
    }
    
}
