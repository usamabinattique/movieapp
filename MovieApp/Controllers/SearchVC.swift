//
//  ViewController.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import UIKit
import SDWebImage

class SearchVC: ParentVC, SearchServiceDelegate{

    //MARK:- IBOutlets
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var searchBarTopLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var pickerView: UIPickerView!

    //MARK:- Properties
    var pageNumber = 1
    var moviesArray = [Movie]()
    var favoritesArray = [Movie]()
    var searchService: SearchService? = nil
    var movieObjectTobeSent: Movie!

    let years = (1940...2018).map { String($0) }
    
    //MARK:- UIController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        customizeUI()
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "showDetailsVC" {

        if let destination = segue.destination as? DetailsVC{
            
                destination.receivedMovieObject = movieObjectTobeSent!
            
            }
        }
    }
    
    
    //MARK:-  Helper Methods
    func customizeUI(){
        
        let backgroundColorView = UIView()
        backgroundColorView.backgroundColor = UIColor.init(white: 1.0, alpha: 0.15)
        UITableViewCell.appearance().selectedBackgroundView = backgroundColorView
        
        searchBarTopLayoutConstraint.constant = 120

        logoLabel.layer.borderWidth = 2.0
        logoLabel.layer.borderColor = UIColor.foregroundColor().cgColor
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.backgroundColor()
        textFieldInsideSearchBar?.textColor = .white
        
        // for setting up the border color of search textfield
        for s in searchBar.subviews[0].subviews {
            if s is UITextField {
                s.layer.borderWidth = 1.0
                s.layer.borderColor = UIColor.foregroundColor().cgColor
            }
        }
    }
    
    @IBAction func filterPressed(_ sender: Any){
        
        view.bringSubview(toFront: pickerView)
    
    }
    
    func searchAPiCall(){
        
        self.showLoadingView("Loading Movie Lists")
        self.searchService = SearchService()
        self.searchService?.initSearchService(queryString: searchBar.text!, pageNumber: pageNumber, delegate: self)
        self.searchService?.getMovies()
    }

    //MARK:-  Search Service Delegate
    func updateSearchServiceSuccess(_ response: [Movie]) {
        
        self.removeLoadingView()

        if response.count > 0 {
            self.moviesArray = response
            
        }else{
            
            self.moviesArray.removeAll()
            self.showAlertView("No search result found for " + searchBar.text!)
            
        }
            self.tableView.reloadData()
    }
    
    func updateSearchServiceFailureWithReason(_ reason: String) {
        
        self.removeLoadingView()
        self.showAlertView(Constants.AlertStrings().somethingWentWrong)
        
    }
    
}


extension SearchVC: UISearchBarDelegate{
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        UIView.animate(withDuration: 0.7, animations: {
            
            self.logoLabel.isHidden = true
            self.searchBarTopLayoutConstraint.constant = 10
            
        }) { (true) in
     
        }
        
        view.sendSubview(toBack: pickerView)
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBarTopLayoutConstraint.constant = 120
        self.moviesArray.removeAll()
        self.tableView.reloadData()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true)
        self.searchAPiCall()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true)
        self.searchAPiCall()
    
    }
}


extension SearchVC: UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moviesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell {
            
            let movieObject = moviesArray[indexPath.row]
            
            cell.configureCell(movieObject: movieObject)
            
            cell.thumbnailImage.sd_setImage(with: URL(string: Constants.serviceURLs().imageBaseUrl + movieObject.posterPath!)) { (image, error, sdf, url) in
                
                cell.thumbnailImage.image = image
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        // the object to be sent to the details screen
        movieObjectTobeSent = moviesArray[indexPath.row]
        performSegue(withIdentifier: "showDetailsVC", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoritesArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
}


extension SearchVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return self.languages.count
        }else{
            return self.years.count
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {

        var title: String
        if component == 0  {
            let key = Array(languages.keys)[row]
            title = languages[key]!
            
        }else{
            
            title =  years[row]
        }
        
        let myTitle = NSAttributedString(string: title, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        return myTitle
    }

 
}

