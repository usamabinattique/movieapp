//
//  DetailsVC.swift
//  MovieApp
//
//  Created by Usama bin Attique on 27/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import UIKit

class DetailsVC: SearchVC {

    @IBOutlet weak var movieArt: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var populairtySeekBar: UIProgressView!
    @IBOutlet weak var ratingsSeekBar: UIProgressView!

    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var releaseYearLabel: UILabel!
    @IBOutlet weak var votesCountLabel: UILabel!
    
    var favButton: UIBarButtonItem!
    var searchVC = SearchVC()
    
    var receivedMovieObject: Movie!

    override func viewDidLoad() {
        super.viewDidLoad()

        customizeUI()
        
//        favButton = searchVC.favButton()
        
        favButton = UIBarButtonItem(image: #imageLiteral(resourceName: "unfavourite"), style: .plain, target: self, action: #selector(favButtonClicked(_:)))
        
        self.navigationItem.rightBarButtonItem = favButton

        descriptionHeightConstraint.constant = descriptionTextView.contentSize.height;
    }
    
    
    @objc func favButtonClicked(_ sender: UIBarButtonItem){
        
        
        
    }
    
    override func customizeUI() {
        
        self.navigationItem.title = "Movie Detail"
        movieTitle.text = receivedMovieObject.title!
        descriptionTextView.text = receivedMovieObject.description!
        languageLabel.text = receivedMovieObject.language!
        
        let dateReceived = Date().getDateFromString(dateString: receivedMovieObject.releaseDate!)
        let year = Date().getYear(dateReceived!)
        releaseYearLabel.text = year

        populairtySeekBar.setProgress(Float(receivedMovieObject.popularityCount!) / 100, animated: true)
        ratingsSeekBar.setProgress(Float(receivedMovieObject.ratings!) / 100, animated: true)
        
        movieArt.sd_setImage(with: URL(string: Constants.serviceURLs().imageBaseUrl + receivedMovieObject.posterPath!)) { (image, error, sdf, url) in
            self.movieArt.image = image
        }
    }

}
