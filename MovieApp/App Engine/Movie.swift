//
//  Movie.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import Foundation

class Movie{
    
    var title: String?
    var releaseDate: String?
    var posterPath: String?
    var language: String?
    var ratings: Double?
    var adultContent: Bool?
    var id: Int?
    var popularityCount: Double?
    var voteCount: Float?
    var description: String?
    
}
