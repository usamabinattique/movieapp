//
//  SearchServiceTranslator.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import Foundation

class SearchServiceTranslator{
    
    class func getSearchResults(_ jsonString: String) -> [Movie] {
        
        var movieArray = [Movie]()
        
        do{
            //try to serialize the JSON
            let dic = try JSONSerialization.jsonObject(with: jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, options: .allowFragments) as! [String:Any]
            
            //Got serialized JSON
            if let resultsDict = dic["results"] as? [[String: Any]] {
                
                for i in 0..<resultsDict.count {
                    
                    // object to add
                    let movie = Movie()
                    
                    // results dictionary
                    let movieDic = resultsDict[i]

                    if let title = movieDic["original_title"] as? String {
                        movie.title = title
                    } else {
                        movie.title = ""
                    }
                    
                    if let original_language = movieDic["original_language"] as? String {
                        movie.language = original_language
                    } else {
                        movie.language = ""
                    }
                    
                    if let ratings = movieDic["vote_average"] as? Double {
                        movie.ratings = ratings
                    } else {
                        movie.ratings = 0
                    }
                    
                    if let poster_path = movieDic["poster_path"] as? String {
                        movie.posterPath = poster_path
                    } else {
                        movie.posterPath = ""
                    }
                    
                    if let adult = movieDic["adult"] as? Bool {
                        movie.adultContent = adult
                    } else {
                        movie.adultContent = false
                    }
                    
                    if let id = movieDic["id"] as? Int {
                        movie.id = id
                    } else {
                        movie.id = 0
                    }
                    
                    if let popularity = movieDic["popularity"] as? Double {
                        movie.popularityCount = popularity
                    } else {
                        movie.popularityCount = 0.0
                    }
                    
                    if let voteCount = movieDic["vote_count"] as? Float {
                        movie.voteCount = voteCount
                    } else {
                        movie.voteCount = 0.0
                    }
                    
                    if let releaseDate = movieDic["release_date"] as? String {
                        movie.releaseDate = releaseDate
                    } else {
                        movie.releaseDate = ""
                    }
                    
                    if let description = movieDic["overview"] as? String {
                        movie.description = description
                    } else {
                        movie.description = ""
                    }
                    
                    movieArray.append(movie)
                }
            }else{
                
                // no results Array
                
            }
        }catch{
            
        }
        return movieArray
    }
}
