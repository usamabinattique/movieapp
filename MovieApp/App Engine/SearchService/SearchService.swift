//
//  SearchService.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import Foundation


protocol SearchServiceDelegate: class {
    func updateSearchServiceSuccess(_ response: [Movie])
    func updateSearchServiceFailureWithReason(_ reason: String)
}


class SearchService: GenericService {
    
    fileprivate weak var delegate: SearchServiceDelegate? = nil
    fileprivate var queryString: String  = ""
    fileprivate var pageNumber: Int = 0
    
    
    func initSearchService(queryString: String, pageNumber: Int, delegate: SearchServiceDelegate) {
        self.delegate = delegate
        self.queryString = queryString
        self.pageNumber = pageNumber
    }
    
    func getMovies() {
        
        
        let urlString =  Constants.serviceURLs().queryString + queryString + Constants.serviceURLs().page + "\(self.pageNumber)"
        
        //Request
        let request = self.createURLRequest(urlString: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, urlResponse, error) in
            if (error != nil) {
                //we got error from service
                
                if let nsError = error as NSError? {
                    if (nsError.code == NSURLErrorTimedOut) {
                    self.requestFailedWithReason(Constants.AlertStrings().timeOutMessage)
                    } else {
                        self.requestFailedWithReason(Constants.AlertStrings().somethingWentWrong)
                    }
                } else {
                    self.requestFailedWithReason(Constants.AlertStrings().somethingWentWrong)
                }
            }else if let httpResponse = urlResponse as? HTTPURLResponse{
                
                //There is no error from server
                //check response status
                
                if httpResponse.statusCode != 200 {
                    //status code is not 200 : failure
                    //call failure block here
                    self.requestFailedWithReason(Constants.AlertStrings().somethingWentWrong)
                    
                }else {
                    
                    //status code is 200
                    //No error
                    //chcek if json is valid and does not contain error key
                    
                    let jsonString = String(data: data!, encoding:String.Encoding.utf8)
                    if(jsonString!.count == 0){
                        
                        //json is not valid
                        //show error message
                        
                        self.requestFailedWithReason(Constants.AlertStrings().somethingWentWrong)
                        
                    }else {

                        let moviesArray = SearchServiceTranslator.getSearchResults(jsonString!)
                        
                        self.requestSucceededWithResponse(moviesArray)
                        
                    }
                }
            }
        }
        task.resume()
    }
    
    
    func requestSucceededWithResponse(_ response: [Movie]) {
        
        DispatchQueue.main.async(execute: {
            
            //delegate
            self.delegate!.updateSearchServiceSuccess(response)
        })
    }
    
    func requestFailedWithReason(_ reason:String){
        
        // call delegate aganist Failed in service response
        
        DispatchQueue.main.async(execute: {
            
            self.delegate!.updateSearchServiceFailureWithReason(reason)
        })
    }
    
    
}
