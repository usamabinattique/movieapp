//
//  GenericService.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import Foundation

class GenericService: Operation{
    
    //MARK:- URL Request
    func createURLRequest(urlString: String) -> URLRequest {
        
        //Request
        let request =  NSMutableURLRequest()
        request.url = URL(string: Constants.serviceURLs().baseUrl + urlString)
        
        //Headers
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "GET"
        
        return request as URLRequest
    }
}
