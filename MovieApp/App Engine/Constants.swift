//
//  Constants.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import Foundation

struct Constants{
    
    struct serviceURLs {
        
        //User
        
        let baseUrl = "http://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838"
        
        let queryString = "&query="
        
        let page = "&page="
        
        let imageBaseUrl = "http://image.tmdb.org/t/p/w92/"

    }
    
    struct AlertStrings {
        
        //Buttons
        let okButton = "OK"
        
        let yesButton = "Yes"
        
        let noButton = "No"
        
        let cancelButton = "Cancel"
        
        //title
        let alertTitle = "MovieApp"
        
        
        //General
        let somethingWentWrong = "Something went wrong while completing your request. Please try again after a little while."
    
        
        //Time Out
        let timeOutMessage = "Looks like the server is taking too long to respond, please try again in sometime."
    
    }
    
}
