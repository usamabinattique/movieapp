//
//  UIColorExtension.swift
//  MovieApp
//
//  Created by Usama bin Attique on 26/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import UIKit

extension UIColor{
    
    static func backgroundColor() -> UIColor {
        return #colorLiteral(red: 0.06274509804, green: 0.09803921569, blue: 0.1215686275, alpha: 1)
    }
    
    static func foregroundColor() -> UIColor {
        return #colorLiteral(red: 1, green: 0.3254901961, blue: 0.1764705882, alpha: 1)
    }
    
}
