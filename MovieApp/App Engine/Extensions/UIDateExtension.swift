//
//  UIDateExtension.swift
//  MovieApp
//
//  Created by Usama bin Attique on 27/09/2018.
//  Copyright © 2018 Usama bin Attique. All rights reserved.
//

import UIKit

extension Date{
    
    func getDateFromString(dateString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.current
        
        return dateFormatter.date(from: dateString) // replace Date String
    }
    
    func getYear(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let strYear = dateFormatter.string(from: date)
        return strYear
    }
}
